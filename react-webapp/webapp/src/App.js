//import logo from './logo.svg';
import MetaMaskConnectionButton from './components/MetaMaskConnectionButton';
import './App.css';

const App = () => {

  return (
    <div className="App">
      <header className="App-header">
        <MetaMaskConnectionButton/>
      </header>
    </div>
  );
}

export default App;
