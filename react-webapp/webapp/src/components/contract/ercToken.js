import { abi } from './abi/ercToken.json'; 
import ContractFactory from './ContractFactory';
import BigNumber from 'bignumber.js';

export default class ERCToken extends ContractFactory {
    constructor({
        // abi,
        address,
        network,
        signer = undefined,
    }) {
        super({
            address, 
            abi, 
            network,
            signer
        });

        // this.abi = abi  // add abi from props

        this.nativeToken = '0x0000000000000000000000000000000000000000';
    };

    /**
     * public method to get the balance of the sender of the token
     * 
     * @param {address} sender 
     * @returns {number} balance
     */

    async getBalance(sender) {
        try {
            if (this.address == this.nativeToken) {
                const ercBalance = this.web3.eth.getBalance(sender)
                return ercBalance
            } else {
                const accountBalance = this.methods.balanceOf(sender).call()
                return accountBalance
            }
        } catch(err) {
            console.log('Error in getting balance', err)
            return err
        }
    };


    /**
     * public method to get the symbol of the token
     * 
     * @returns {string} token symbol
     */
    async getSymbol() {
        try {
            if (this.address == this.nativeToken) {
                return 'ETH'
            } else {
                return await this.methods.symbol().call()  
            }
        } catch(err) {
            console.log('Error in getting symbol', err)
            return err
        }
    };


    /**
     * public method to get the decimals of the token
     * 
     * @returns {number} decimals
     */
    async getDecimals() {
        try {
            if (this.address == this.nativeToken) {
                return 18;
            } else {
                return Number(await this.methods.decimals().call());
            };
        } catch(err) {
            console.log('Error in getting decimals', err)
            return err
        }
    };

    /**
     * public method to create token transfer raw txn
     * 
     * @param {address} recipient 
     * @param {string} amount 
     * @returns {object} unsigned transaction
     */
    async send(recipient, amount) {
        const payload = this.methods.transfer(recipient, amount).encodeABI();
        console.log('Send', payload)
        const unsignedTx = await this.txFactory(payload,)
        return unsignedTx
    };

    async getAllowance (ownerAddress, spenderAddress, decimals){
        try {
            let allowance = await this.methods.allowance(ownerAddress, spenderAddress).call();
            allowance = new BigNumber((allowance).toString())
            allowance = allowance.shiftedBy(-decimals)
            console.log('getAllowance', allowance)
            return allowance;
        } catch(err){
            console.log('Error in getting allowance', err)
        }
    }


    /**
     * public method to create token transfer raw txn
     * 
     * @param {number} inputAmount 
     * @param {number} allowance 
     * @param {string} spenderAddress
     * @param {number} tokenDecimals
     * @returns {number} approval amount
     */

    async getSpenderApproval(spenderAddress, tokenDecimals){
        try {
                let value = new BigNumber("999999");
                let decimals = Math.pow(10, tokenDecimals);
                value = value.multipliedBy(decimals); 
                const approvalResult = await this.methods.approve(spenderAddress,value.toFixed()).encodeABI();
                console.log('approved', approvalResult)
                return approvalResult;            
        } catch (err) {
            console.log('Error in getting spender approval', err)
        }
    };

}