import Web3 from "web3";
// import bitePointABI from './abi/bitePoint.json';

export default class ContractFactory { //extends Web3GlobalProviderController
    constructor({
        address, //user account
        abi,  
        network = 137,
        signer = undefined,
        web3 = new Web3(window.ethereum)
    }){
        this.web3 = web3
        this.address = Web3.utils.toChecksumAddress(address);
        this.abi = abi;
        this.network = network;
        this.contract = new this.web3.eth.Contract(this.abi, this.address) //e.g. BITEPT ABI + ADDRESS
        this.methods = this.contract.methods;
        this.signer = signer;

        //0x64a43a3E9F08629499CeD15BdF3e0dB26fba5550 BITE_PT
        //POOL_CONTRACT
        // console.log('ContractFactory',this.address , this.signer.selectedAddress, this.web3.eth.currentProvider.selectedAddress)
    }

    async getNonce() {
        return await this.methods.nonces(this.signer.selectedAddress).call()
        // return this.web3.eth.getTransactionCount('0x5275a5305D140a2f61Fe28fE2aee85cCF11dBa02')
    };


      
}




























//     async getTokenBalance(address, decimals = null) {
//     const contract = await getWildCardTokenContract(address);
//     const balance = await this._getBalance(contract);
//     return balance
//   };

//     async bigNumToInt(hex) {
//         const web3 = new Web3(window.ethereum);
//         const numValue = await web3.utils.hexToNumberString(hex)
//         return numValue
//     }

//     async parseBigNumValue(hexVal, divider) {
//     const web3 = new Web3(window.ethereum);
//     let value = await web3.utils.hexToNumberString(hexVal) //{val: "999999000000"}
//     return parseInt(value) / Math.pow(10, divider);
//   };
