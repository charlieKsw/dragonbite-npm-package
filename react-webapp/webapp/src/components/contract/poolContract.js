// import { abi } from './abi/poolContract.json'; //Pool Contract ABI
// import bitePointABI from './abi/bitePoint.json';
import ContractFactory from './ContractFactory';
import BigNumber from 'bignumber.js';

const BITE_PT_ADDRESS = '0x9ec1f605588c7f0774A4DE46742C7681b0B459d9'
const BITE_ADDRESS = '0x280724409b288de06c6d66c05965d3d456e2283a'
const USDT_ADDRESS = '0xc2132d05d31c914a87c6611c10748aeb04b58e8f'
const WETH_ADDRESS = '0x7ceb23fd6bc0add59e62ac25578270cff1b9f619'
const POOL_CONTRACT_ADDRESS = '0xA3e1DaF9F5f4b1c8CCf585Aa7629157e67BC37c5'

export default class PoolContract extends ContractFactory {    
    constructor({ 
        address,
        abi,
        network,
        signer = undefined,
    }){
        // const address = "0x64a43a3E9F08629499CeD15BdF3e0dB26fba5550" // BITE_PT POLYGON
        super({ address, abi , network, signer }) //extend from the parent
        this.tokenName = 'Dragonbite Point';
        this.address = address
        this.abi = abi;
        console.log('PoolContract', this.abi)


    this.nativeToken = '0x0000000000000000000000000000000000000000'

    }
    
    parseBigNumValue() {
        return this._parseBigNumValue
    }

    _parseBigNumValue(balance, divider){
        let bnToStringValue = new BigNumber(balance).shiftedBy(-divider).toString()
        return bnToStringValue
    }
    async _getTokenContract(){
        // return new ethers.Contract(this.address, this.abi);
        const contractClass = new this.web3.eth.Contract(this.abi, this.address)
        console.log('contractClass', contractClass)
        return contractClass
    }

    async _getTokenBalance(){
        let tokenBalance = await this.methods.balanceOf(this.web3.currentProvider.selectedAddress).call()
        return tokenBalance;
    }


    async getAllBalance(){
        let tokenAddress = [BITE_PT_ADDRESS, BITE_ADDRESS, USDT_ADDRESS, WETH_ADDRESS]
        let balancelist = []
        let decimals = 18;
        let symbol = 'MATIC';
        let allowance = -1;
        let balance = 0;

        if (tokenAddress !== this.nativeToken) {
           const tokenContract = await this._getTokenContract(tokenAddress);

            symbol = await tokenContract.getSymbol();
            console.log('SSSSS', symbol)
            // balance = await this._getTokenBalance() //get each token balance
            // decimals = await tokenContract.decimals();
            // allowance = await tokenContract.allowance(window.ethereum.selectedAddress, POOL_CONTRACT_ADDRESS);
            // console.log('params', {symbol, balance, decimals, allowance})

            //allowance = await parseBigNumValue(allowance, decimals);
          }
        
        // else case
        //   const mapper = {
        //     decimals: decimals,
        //     symbol: symbol,
        //     allowance, allowance,
        //     address: tokenAddress,
        //     balance: balance,
        //   };

        //    balancelist = [...balancelist, mapper];
      
        return balancelist;
    }

    async getBalance(){
       try {
            let balance = await this.methods.balanceOf(this.web3.currentProvider.selectedAddress).call() //get all accounts balance 
            let decimals = await this.methods.decimals().call();
            const parsedBnValue = await this._parseBigNumValue(balance, Number(decimals));
            return parsedBnValue;
        } catch(error){
            throw Error("Error in getting balance")
        }
    }

    // async getNonce() {
    //     const tokenContract = new ContractFactory({
    //         address: "0x64a43a3E9F08629499CeD15BdF3e0dB26fba5550" ,
    //         abi: bitePointABI.abi,
    //         network: this.network,
    //         signer: this.signer,
    //     });
    //     let nonce = await tokenContract.methods.nonces(this.signer.address).call();
    //     return nonce;
    // }
    
    async createPermitMessageData(
        fromAddress,
        spender,// POOL contract
        nonce,
        value,
        deadline) {
      
        const message = {
          owner: fromAddress,
          spender: spender,
          value: value,
          nonce: nonce,
          deadline: deadline
        };
      
        const typedData = JSON.stringify({
          types: {
            EIP712Domain: [
              {
                name: "name",
                type: "string",
              },
              {
                name: "version",
                type: "string",
              },
              {
                name: "chainId",
                type: "uint256",
              },
              {
                name: "verifyingContract",
                type: "address",
              },
            ],
            Permit: [
              {
                name: "owner",
                type: "address",
              },
              {
                name: "spender",
                type: "address",
              },
              {
                name: "value",
                type: "uint256",
              },
              {
                name: "nonce",
                type: "uint256",
              },
              {
                name: "deadline",
                type: "uint256",
              }
            ],
          },
          primaryType: "Permit",
          domain: {
            name: "Dragonbite Point",
            version: "1",
            chainId: 137,
            verifyingContract: BITE_PT_ADDRESS
          },
          message: message,
        });
      
        return {
          typedData,
          message,
        };
      };

    async signData (fromAddress, typeData) {
    return new Promise(function (resolve, reject) {
        window.ethereum.sendAsync(
            {
                id: 1,
                method: "eth_signTypedData_v3",
                params: [fromAddress, typeData],
                from: fromAddress,
            },
            function (err, result) {
                if (err) {
                    console.log('Error in signing data', err)
                    reject(err)
                } else {
                    const r = result.result.slice(0, 66);
                    const s = "0x" + result.result.slice(66, 130);
                    const v = Number("0x" + result.result.slice(130, 132));
                    resolve({
                        v,
                        r,
                        s,
                    });
                }
            }
        );
    })
    };
    
    // async postTransactionData(
    //             orderId,
    //             checkSumAddress,
    //             amountTransfer, // Approve Amount 
    //             amountTransfer, // Deposit Amount
    //             now,
    //             order.payment_object_mongo_id,
    //             sign.v,
    //             sign.r,
    //             sign.s
    //             ){

    //     const postResult = postTransactionData()
    //     return await postResult
    // }

}
