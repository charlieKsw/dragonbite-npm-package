import { ETHEREUM_SVG } from './ethereum.svg';
import { KOVAN_SVG } from './kovan.svg';
import { POLYGON_SVG } from './polygon.svg';

export const SVGs = () => {
    return { ETHEREUM_SVG, KOVAN_SVG, POLYGON_SVG }
}
