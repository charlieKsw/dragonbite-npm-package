// import * as POLYGON_SVG from '../../assets/polygon.svg';
// import * as KOVAN_SVG from '../../assets/kovan.svg';
// import * as ETHEREUM_SVG from '../../assets/ethereum.svg';

import { POLYGON_SVG, KOVAN_SVG, ETHEREUM_SVG } from '../../assets/index';
// console.log('CONFIG', POLYGON_SVG, KOVAN_SVG, ETHEREUM_SVG )

export const ADDRESSES = {
  POLYGON: {
    BITEPOINT: {
      type: 'token',
      address: '0x64a43a3E9F08629499CeD15BdF3e0dB26fba5550',
      symbol: 'BITEPOINT',
      decimals: 18
    },
  },
}

export const networkMapping = {
  1: {
    param: {
      chainId: '0x1',
      chainName: 'Ethereum Main Network (Mainnet)',
    },
    meta: {
      svg: () => ETHEREUM_SVG,
      name: `Ethereum`
    }
  },
  42: {
    param: {
      chainId: '0x2a',
      chainName: 'Kovan Test Network',
    },
    meta: {
      svg: () => KOVAN_SVG,
      name: `Kovan`
    }
  },
  137: {
    param: {
      chainId: '0x89',
      chainName: 'Polygon Mainnet',
      nativeCurrency: {
        name: 'matic',
        symbol: 'MATIC',
        decimals: 18
      },
      rpcUrls: ['https://polygon-rpc.com/'],
      blockExplorerUrls: ['https://polygonscan.com/']
    },
    meta: {
      svg: () => POLYGON_SVG,
      name: `Polygon`
    }
  }
}