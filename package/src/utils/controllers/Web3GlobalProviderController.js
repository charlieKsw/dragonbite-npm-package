import Web3 from "web3";
import BigNumber from "bignumber.js";
import networkMapping from './config';

export default class Web3GlobalProviderController {
    constructor({
        provider=window.ethereum,
        network
    }) {
        this.web3 = new Web3(window.ethereum); //default window.ethereum = this.provider
        this.provider = provider
        this.network = network

        console.log('Web3GlobalProviderController', this)
    };

    parseBigNumValue() {
        return this._parseBigNumValue
    }

    _parseBigNumValue(balance, divider){
        let bnToStringValue = new BigNumber(balance).shiftedBy(-divider).toString()
        return bnToStringValue
    }

    async isMetaMaskConnected(){
        const accounts = await this.web3.eth.getAccounts() //check whether Metamask is connected 
        if (accounts.length == 0) {
          console.log("User is not logged in to MetaMask");
          return false 
        }
        else {
          console.log("User is logged in to MetaMask");
          return true
        }
    }
    
    async _connectMetamask(){
      return await window.ethereum.request({ method: 'eth_requestAccounts' });
    }

    async _handleChainlabel (chainId){
      if(!chainId) return {}

      let chainIdObject = {}
      let label = 'mainnet'
      switch(chainId){
        case 1:
          label = 'Ethereum'
          break;
        case 42:
          label = 'Kovan'
          break;
        case 137:
          label = 'Polygon'
          break;
        default:
      }
      const chainIdInfo = {...chainIdObject, chainId: chainId, label: label}
      return chainIdInfo
    } 

    async getChainId() {
      const chainId = await this.web3.eth.getChainId()
      const updateChainIdInfo = await this._handleChainlabel(chainId)
      return updateChainIdInfo;
    };

    async _getUserProfile(){
      let userProfile = {}
      if(this.web3){
        let accounts = await this.getCurrentSignerAddress()
        let currentChainId = await this.getChainId()
          userProfile = 
            { ...userProfile,
              account: accounts, 
              chainId: currentChainId['chainId'],
              label: currentChainId['label']
            }
        return userProfile 
      }
      return "Error in getting user profile"
    }


    async handleMetamaskConnect() {
      if (!window.ethereum || !window.ethereum.isMetaMask) { // Check is Metamask is installed
        throw Error('Metamask not installed')
      } else if (!window.ethereum) {
        throw Error("Error in connecting Metamask")
      } else {
        console.log('Connected to Metamask') // Return connected signerAccount ["address"]
      }
        const connectAccount = await this._connectMetamask() // Wallet connected
        const updatedUserProfile = await this._getUserProfile() // To update user full profile
        return updatedUserProfile
    };

    async getAddress(){ // Get all addresses
      return await this.web.eth.getAddress()
    }

    async getCurrentSignerAddress(){  // Get current connected addresses
      if(!window.ethereum) {
        throw Error("Missing provider, please make sure Metamask is connected")
      }
      const currentSignerAddress = await this.web3.eth.currentProvider.selectedAddress 
      return currentSignerAddress;
    }
    
    async addTokenClick(
      address = '0x7ceb23fd6bc0add59e62ac25578270cff1b9f619',
      symbol = 'WETH',
      decimals = 18,
    ) {
        try {
            const watchAsset = await ethereum.request({
                method: 'wallet_watchAsset',
                params: {
                    type: 'ERC20',
                    options: {
                        address: address,
                        symbol: symbol,
                        decimals: decimals,
                        image: '',
                    },
                },
            })
            if (watchAsset || watchAsset.success) {
                console.log('Added token success')
            } else {
                console.log('Failed to add token')
            }
        } catch (err) {
            console.log('Error in adding asset token to Metamask', err)
        }
    }

    abbreviateAddress(address) {
      return address.substr(0, 6) + '...' + address.substr(address.length - 4, 4);
    };

    async switchNetwork(chainId) {
      if (!chainId) {
          throw Error('chainId is missing')
      };
      console.log('Switching network:', chainId)
      try {
          const data = [{ chainId: chainId }]
          const switchedNetwork = await window.ethereum.request({
              method: 'wallet_switchEthereumChain',
              params: data,
          });
          if (switchedNetwork) return null

      } catch (switchError) {
          console.log('Error in switching polygon', switchError)
          if (switchError.code === 4902) { // 4902 indicates the chain has not been added to MetaMask
              const param = networkMapping[Number(chainId)].param;
              try {
                  await ethereum.request({
                      method: 'wallet_addEthereumChain',
                      params: [param],
                  });
              } catch (err) {
                  console.log('Error in adding ethereum chain', err)
              }
          }
      }
  };


};

