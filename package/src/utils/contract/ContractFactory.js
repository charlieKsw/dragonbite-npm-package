import Web3GlobalProviderController from '../controllers/Web3GlobalProviderController';
import Web3 from "web3";

export default class ContractFactory extends Web3GlobalProviderController {
    
    constructor({
        address, 
        abi,
        network,
        provider
    }){
        super({provider, network})
        this.address = Web3.utils.toChecksumAddress(address);
        this.abi = abi;
        this.contract = new this.web3.eth.Contract(this.abi, this.address)
        this.methods = this.contract.methods; 
    }

    async getNonce() {
        return await this.methods.nonces(this.provider.selectedAddress).call()
    };    
}
