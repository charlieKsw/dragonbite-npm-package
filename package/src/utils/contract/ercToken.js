import ContractFactory from './ContractFactory';
import BigNumber from 'bignumber.js';

const BITE_PT_ADDRESS = '0x64a43a3E9F08629499CeD15BdF3e0dB26fba5550'
const BITE_ADDRESS = '0x280724409b288de06c6d66c05965d3d456e2283a'
const USDT_ADDRESS = '0xc2132d05d31c914a87c6611c10748aeb04b58e8f'
const WETH_ADDRESS = '0x7ceb23fd6bc0add59e62ac25578270cff1b9f619'
const POOL_CONTRACT_ADDRESS = '0xA3e1DaF9F5f4b1c8CCf585Aa7629157e67BC37c5'

export default class ERCToken extends ContractFactory {
    constructor({
        address,
        abi,
        network,
    }) {
        super({
            address, 
            abi, 
            network
        });
        this.abi = abi 
        this.nativeToken = '0x0000000000000000000000000000000000000000';
    };

    /**
     * public method to get the balance of the sender of the token
     * 
     * @param {address} sender 
     * @returns {number} balance
     */

    async getBalance(sender) {
        try {
            if (this.address == this.nativeToken) {
                const ercBalance = this.web3.eth.getBalance(sender)
                return ercBalance
            } else {
                const accountBalance = this.methods.balanceOf(sender).call()
                return accountBalance
            }
        } catch(err) {
            console.log('Error in getting balance', err)
            return err
        }
    };


    /**
     * public method to get the symbol of the token
     * 
     * @returns {string} token symbol
     */
    async getSymbol() {
        try {
            if (this.address == this.nativeToken) {
                return 'ETH'
            } else {
                return await this.methods.symbol().call()  
            }
        } catch(err) {
            console.log('Error in getting symbol', err)
            return err
        }
    };


    /**
     * public method to get the decimals of the token
     * 
     * @returns {number} decimals
     */
    async getDecimals() {
        try {
            if (this.address == this.nativeToken) {
                return 18;
            } else {
                return Number(await this.methods.decimals().call());
            };
        } catch(err) {
            console.log('Error in getting decimals', err)
            return err
        }
    };

    /**
     * public method to create token transfer raw txn
     * 
     * @param {address} recipient 
     * @param {string} amount 
     * @returns {object} unsigned transaction
     */
    async send(recipient, amount) {
        const payload = this.methods.transfer(recipient, amount).encodeABI();
        console.log('Send', payload)
        const unsignedTx = await this.txFactory(payload,)
        return unsignedTx
    };

    async getAllowance (ownerAddress, spenderAddress, decimals){
        // console.log({ownerAddress, spenderAddress, decimals})
        try {
            let allowance = await this.methods.allowance(ownerAddress, spenderAddress).call();
            allowance = new BigNumber((allowance).toString())
            allowance = allowance.shiftedBy(-decimals)
            return allowance;
        } catch (err){
            console.log('Error in getting allowance', err)
        }
    }
    /**
     * public method to create token transfer raw txn
     * 
     * @param {number} inputAmount 
     * @param {number} allowance 
     * @param {string} spenderAddress
     * @param {number} tokenDecimals
     * @returns {number} approval amount
     */

    async getSpenderApproval(spenderAddress, tokenDecimals){
        try {
                let value = new BigNumber("999999");
                let decimals = Math.pow(10, tokenDecimals);
                value = value.multipliedBy(decimals); 
                const approvalResult = await this.methods.approve(spenderAddress,value.toFixed()).encodeABI();
                console.log('approved', approvalResult)
                return approvalResult;            
        } catch (err) {
            console.log('Error in getting spender approval', err)
        }
    };

    // MetaMask Token Balance 
    async getAllBalance() {
        let tokenAddress = [BITE_PT_ADDRESS, BITE_ADDRESS, USDT_ADDRESS, WETH_ADDRESS]
        let tknList = tokenAddress.map((address) => {
            return {
                address: address, 
                balance: 0,
                symbol: '',
                decimals: 0,
            }
        })
        return await this._getBalance(tknList, false);
    };
  
      
    async _getBalance(tokenList, balanceOnly=true) {
        const walletAddress = this.web3.currentProvider.selectedAddress
        let network = this.network 
        let abi = this.abi
        if(tokenList){
            return Promise.all(tokenList.map(async (tkn) => {
                let balance,
                    symbol,
                    decimals;
                let address = tkn.address;
                const args = { address, network, abi };
                const token = new ERCToken(args); 
                symbol = balanceOnly ? tkn.symbol : await token.getSymbol();
                balance = await token.getBalance(walletAddress);
                decimals = balanceOnly ? tkn.decimals : await token.getDecimals()
                balance = new BigNumber(balance)
                balance = balance.shiftedBy(-decimals)
                balance = balance.toNumber()
                return { address, balance, symbol, decimals}
            }))
            .then(results => {return results})
        }
        return []
    }

}