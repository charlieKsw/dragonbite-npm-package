import ContractFactory from './ContractFactory';
import BigNumber from 'bignumber.js';

const BITE_PT_ADDRESS = '0x64a43a3E9F08629499CeD15BdF3e0dB26fba5550'
const BITE_ADDRESS = '0x280724409b288de06c6d66c05965d3d456e2283a'
const USDT_ADDRESS = '0xc2132d05d31c914a87c6611c10748aeb04b58e8f'
const WETH_ADDRESS = '0x7ceb23fd6bc0add59e62ac25578270cff1b9f619'
const POOL_CONTRACT_ADDRESS = '0xA3e1DaF9F5f4b1c8CCf585Aa7629157e67BC37c5'

export default class PoolContract extends ContractFactory {
    
    constructor({
        address, 
        abi, 
        network,
        signer = undefined,
    }){
        super({address, abi, network, signer})
        this.tokenName = 'Dragonbite Point';
        this.abi = abi
        this.address = address
    }

    async _getTokenContract(){
        return new this.web3.eth.Contract(this.abi, this.address)
    }

    async _getTokenBalance(){
        let tokenBalance = await this.methods.balanceOf(this.web3.currentProvider.selectedAddress).call()
        return tokenBalance;
    }

    async getTokenBalance(){
       try {
            let balance = await this.methods.balanceOf(this.web3.currentProvider.selectedAddress).call()
            let decimals = await this.methods.decimals().call();
            const parsedBnValue = await this._parseBigNumValue(balance, Number(decimals));
            return parsedBnValue;
        } catch(error){
          console.log('Error in getting balance', error)
        }
    }

    async createPermitMessageData(
        fromAddress,
        spender,// POOL contract
        nonce,
        value,
        deadline) {
      
        const message = {
          owner: fromAddress,
          spender: spender,
          value: value,
          nonce: nonce,
          deadline: deadline
        };
      
        const typedData = JSON.stringify({
          types: {
            EIP712Domain: [
              {
                name: "name",
                type: "string",
              },
              {
                name: "version",
                type: "string",
              },
              {
                name: "chainId",
                type: "uint256",
              },
              {
                name: "verifyingContract",
                type: "address",
              },
            ],
            Permit: [
              {
                name: "owner",
                type: "address",
              },
              {
                name: "spender",
                type: "address",
              },
              {
                name: "value",
                type: "uint256",
              },
              {
                name: "nonce",
                type: "uint256",
              },
              {
                name: "deadline",
                type: "uint256",
              }
            ],
          },
          primaryType: "Permit",
          domain: {
            name: "Dragonbite Point",
            version: "1",
            chainId: 137,
            verifyingContract: BITE_PT_ADDRESS
          },
          message: message,
        });
      
        return {
          typedData,
          message,
        };
      };

    async signData (fromAddress, typeData) {
    return new Promise(function (resolve, reject) {
        window.ethereum.sendAsync(
            {
                id: 1,
                method: "eth_signTypedData_v3",
                params: [fromAddress, typeData],
                from: fromAddress,
            },
            function (err, result) {
                if (err) {
                    console.log('Error in signing data', err)
                    reject(err)
                } else {
                    const r = result.result.slice(0, 66);
                    const s = "0x" + result.result.slice(66, 130);
                    const v = Number("0x" + result.result.slice(130, 132));
                    resolve({
                        v,
                        r,
                        s,
                    });
                }
            }
        );
    })
    };
    

}
